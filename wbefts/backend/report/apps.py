from django.apps import AppConfig


class ReportConfig(AppConfig):
    name = 'wbefts.backend.report'
