from django.conf.urls import url, include
from . import views

file_patterns = [
    url(r'^$', views.index, name="index"),
]

letter_patterns = [
    url(r'^$', views.index, name="index"),
]

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^file/', include(file_patterns, namespace="file")),
    url(r'^letter/', include(letter_patterns, namespace="letter")),
]
