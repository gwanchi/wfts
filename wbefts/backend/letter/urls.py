from django.conf.urls import url, include
from . import views

process_patterns = [
    url(r'^$', views.index, name="index"),
]

register_patterns = [
    url(r'^$', views.index, name="index"),
]

storage_patterns = [
    url(r'^$', views.index, name="index"),
]

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^register/', include(register_patterns, namespace="register")),
    url(r'^process/', include(register_patterns, namespace="process")),
    url(r'^storage/', include(storage_patterns, namespace="storage")),
]
