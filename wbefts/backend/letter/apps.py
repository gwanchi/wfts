from django.apps import AppConfig


class LetterConfig(AppConfig):
    name = 'wbefts.backend.letter'
