from django.conf.urls import url, include
from . import views

api_patterns = [
    url(r'^$', views.index, name="index"),
]

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^api/', include(api_patterns, namespace="api")),
]
