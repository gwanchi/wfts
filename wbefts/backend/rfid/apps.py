from django.apps import AppConfig


class RfidConfig(AppConfig):
    name = 'wbefts.backend.rfid'
