from django.apps import AppConfig


class FileConfig(AppConfig):
    name = 'wbefts.backend.file'
