from django.shortcuts import render
from django.core.urlresolvers import reverse

# Create your views here.


def index(request):
    # Content from request or database extracted here
    # and passed to the template for display
    return render(request, 'backend/file/index.html')