from django.apps import AppConfig


class DashboardConfig(AppConfig):
    name = 'wbefts.backend.dashboard'
