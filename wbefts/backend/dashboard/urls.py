from django.conf.urls import url, include
from . import views

summary_patterns = [
    url(r'^$', views.index, name="index"),
]

urlpatterns = [
    url(r'^', include(summary_patterns)),
]
