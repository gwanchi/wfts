from django.apps import AppConfig


class SysdefConfig(AppConfig):
    name = 'wbefts.backend.sysdef'
