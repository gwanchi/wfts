from django.conf.urls import url, include
from . import views

codes_patterns = [
    url(r'^$', views.index, name="index"),
]

urlpatterns = [
    url(r'^$', views.index, name="index"),
    url(r'^codes/', include(codes_patterns, namespace="codes")),
]
