from django.conf.urls import url, include
from . import views

login_patterns = [
    url(r'^$', views.index, name="index"),
]

urlpatterns = [
    url(r'^', include(login_patterns)),
]
