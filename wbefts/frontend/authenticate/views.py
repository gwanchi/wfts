from django.shortcuts import render
from django.http import HttpResponse
from django.core.urlresolvers import reverse

# Create your views here.


def index(request):
    # Content from request or database extracted here
    # and passed to the template for display
    STORE_NAME = 'Downtown'
    store_address = {'street': 'Main #385', 'city': 'San Diego', 'state': 'CA'}
    store_amenities = ['WiFi', 'A/C']
    store_menu = ((0, ''), (1, 'Drinks'), (2, 'Food'))
    values_for_template = {'store_name': STORE_NAME, 'store_address': store_address, 'store_amenities': store_amenities,
                           'store_menu': store_menu}
    return render(request, 'frontend/authenticate/index.html', values_for_template)
