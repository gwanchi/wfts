from django.apps import AppConfig


class AuthenticateConfig(AppConfig):
    name = 'wbefts.frontend.authenticate'
